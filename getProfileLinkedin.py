# %%
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
import csv

options = Options()
options.page_load_strategy = 'normal'
# driver = webdriver.Chrome(options=options)



# Step1: Open Chrome and Login to Linked
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),options=options)

# driver = webdriver.Chrome(ChromeDriverManager().install())
url = "https://www.linkedin.com/login"
driver.get(url)

with open("login_credential.txt", "r") as f:
    line = f.readlines()
username = line[0].split()
password = line[1].split()

def login_linked(username,password):
    username_field = driver.find_element_by_xpath('//*[@id="username"]').send_keys(username)
    sleep(3)
    password_field = driver.find_element_by_xpath('//*[@id="password"]').send_keys(password)

    sleep(3)
    button_sign_in_field = driver.find_element_by_xpath('//*[@id="organic-div"]/form/div[3]/button')
    webdriver.ActionChains(driver).double_click(button_sign_in_field).perform()
    url = driver.current_url
    if 'checkpoint' in url:
        code = input('insert code:')
        code_input = driver.find_element(By.ID,'input__email_verification_pin').send_keys(code)
        submit_field = driver.find_element(By.ID,'email-pin-submit-button')
        webdriver.ActionChains(driver).click(submit_field).perform()

# Step 2: Search for the profile we want to crawl
def search_for_the_profile():
    sleep(3)
    # Locate the search bar element
    search_field = driver.find_element_by_xpath('//*[@id="global-nav-typeahead"]/input')

    # Input the search query tho the search ar
    sleep(4)
    # search_query = input("What profile do you want to search: ")
    search_query = 'software enginner'
    search_field.send_keys(search_query)
    sleep(3)
    search_field.send_keys(Keys.ENTER)
    print('-Finish searching ...')
    sleep(3)
    # people_field = driver.find_element(By.XPATH,'//*[@id="search-reusables__filters-bar"]/ul/li[1]/button')
    people_field = driver.find_element(By.XPATH,'//button[@aria-label="People"]')
    webdriver.ActionChains(driver).click(people_field).perform()
# Step 3: 
def get_url(): 
    page_source = BeautifulSoup(driver.page_source, 'html.parser')
    # link_profile= page_source.find_all('a',attrs={"class": "app-aware-link"})
    # artdeco_card = page_source.find_all('div',class_='ph0 pv2 artdeco-card mb2')
    # artdeco_card_source = BeautifulSoup(str(artdeco_card[0]),'html.parser')
    link_profiles = page_source.find_all('a',class_='app-aware-link',attrs={'aria-hidden':'true'})
    list_profileid = [] 
    for profileid in link_profiles:
        if profileid.get('href') not in list_profileid:
            list_profileid.append(profileid.get('href'))

    return list_profileid 
def get_url_son_pages():
    range_number = int(input("How many pages you want to scrape: "))
    url_all_page=[]
    for page in range(range_number):
        driver.execute_script('window.scroll(0,document.body.scrollHeight)')
        url_one_page = get_url()
        sleep(2)
        next_button = driver.find_element(By.XPATH,'//button[@aria-label="Next"]').click()
        url_all_page = url_all_page +url_one_page
        sleep(2)
    return url_all_page

def output_csv(url_all_pages):
    with open('output.csv','w',newline='') as file_output:
        headers = ['Name','Job title','Location','Url']
        writer = csv.DictWriter(file_output,delimiter=',',lineterminator='\n',fieldnames=headers)
        writer.writeheader()
        for link_url in url_all_pages:
            driver.get(link_url)
            page_source = BeautifulSoup(driver.page_source,'html.parser')
            sleep(2)
            info_div = page_source.find('div', class_='ph5 pb5') 

            name = info_div.find('h1',class_='text-heading-xlarge').text.strip()
            job_title = info_div.find('div',class_ = 'text-body-medium').text.strip()
            location_profile = info_div.find('span',class_ ='text-body-small inline t-black--light break-words').text.strip()
            print( name +' | '+job_title + ' | ' +location_profile)
            
            writer.writerow({
                headers[0]:name,
                headers[1]:location_profile,
                headers[2]:job_title,
                headers[3]: link_url,
            })

login_linked(username,password)    
search_for_the_profile()
url_all_pages = get_url_son_pages()
output_csv(url_all_pages)
# driver.quit()


